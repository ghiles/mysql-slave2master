#!/bin/bash

###################################################
#Author Ghiles KHEDDACHE
#
###################################################
#set -x
#NEW_MASTER=$0
#OLD_MASTER=$1
USER='root'
PASSWORD='Password'
CREDENTIAL="-u$USER -p$PASSWORD "
REPLICATION_USER="centreon-repl"
REPLICATION_PASS="Password"
SKIP_TEST=0
CONFIG=0
#/bin/bash

usage()
{
 echo
 echo "Usage: $0 [-m <alias bdd new master>] [-s <alias bdd new slave>]" 1>&2
 exit 1
}

check_mysql_connect()
{
        mysql -f -h "$1"  $CREDENTIAL -e "quit"
        if [ "$?" -ne 0 ] ; then
		
                echo "Connexion mysql echouée, serveur:'$1'" >&2
                exit 2
	else 
	  echo " Connect to BDD $1...: SUCCESS"
        fi
        return 0
}

get_actual_config()
{
echo
# TO DO: Show the actual topology master and slave
}

test_replication()
{
echo "# Creating a table in the master"
mysql -f -h "$NEW_MASTER" $CREDENTIAL -e 'create schema if not exists test'
mysql -f -h "$NEW_MASTER" $CREDENTIAL -ve 'drop table if exists test.t1'
RAND_NUM=$RANDOM
mysql -f -h "$NEW_MASTER" $CREDENTIAL -ve ' create table t1 (RNDM int);' test
mysql -f -h "$NEW_MASTER" $CREDENTIAL -ve " insert into t1 value ($RAND_NUM);" test
echo "Waiting 10s for replication"
sleep 10
exit_code=0
echo "# Retrieving the table from the slaves"
mysql -f -h "$OLD_MASTER" $CREDENTIAL -e 'select * from test.t1'

REPLICATED=$(mysql -f -h "$OLD_MASTER" $CREDENTIAL -e 'select RNDM from test.t1 \G' | grep RNDM | awk '{ print $2 }')
    if [ "$REPLICATED" == "$RAND_NUM"  ]
    then
        echo "OK - Slave $OLD_MASTER has replicated table t1"
    else
        echo "NOT OK - Slave $OLD_MASTER has NOT replicated table t1"
        exit_code=1
    fi 
}

IS_SLAVE()
{

                SLAVE_IO=$(mysql -f -h "$1" $CREDENTIAL -e "SHOW SLAVE STATUS\G" | grep "Slave_IO_Running" | awk '{ print $2 }')
                SLAVE_SQL=$(mysql -f -h "$1" $CREDENTIAL -e "SHOW SLAVE STATUS\G" | grep "Slave_SQL_Running" | awk '{ print $2 }')
                if [ -z  "$SLAVE_IO" ] && [ -z "$SLAVE_SQL" ] ; then
                        echo "$1 is not a slave !"
			return 1;

                else
			echo "$1 is a slave, checking his status"
			echo "Slave_IO_Running : $SLAVE_IO"
			echo "Slave_SQL_Running : $SLAVE_SQL"
                        if [ "$SLAVE_IO" == "Connecting" ] && [ "$SLAVE_SQL" == "Yes" ] ; then 
				echo " SLAVE is trying to  connect to the MASTER..check if master is up"	
			fi 

                     return 0;
                fi
}
switch_slave_master()
{


        # Verification du slave:
		if  IS_SLAVE "$NEW_MASTER" ; then
                 OLD_SLAVE="$NEW_MASTER"
                
        		# check if we have two slave 
               		if IS_SLAVE "$OLD_MASTER" ; then
                  	echo "two slave found, need to clean manually..exit ! "
                  	# statement ERROOR because two master.
                  	exit 2
                	fi
               fi 


        # For the master, Who is the real slave ?
		CURRENT_SLAVE=$(mysql -f -h $OLD_MASTER $CREDENTIAL -e "SHOW PROCESSLIST" | grep -i "Binlog Dump" | awk '{ print $3 }'| awk -F\: '{ print $1 }')
				echo "getting slave host from master...:  $CURRENT_SLAVE"
        # For the slave, who is the current Master
		CURRENT_MASTER=$(mysql -f -h "$NEW_MASTER" $CREDENTIAL -e "SHOW SLAVE STATUS\G" | grep -i "Master_Host" | awk '{ print $2 }')
				echo "getting Master host from the slave...:  $CURRENT_MASTER"
		  if [ "$CURRENT_MASTER" == "$NEW_MASTER" ] ; then 
		     echo "$NEW_MASTER is already  master! exiting...."
		     exit 2;
		  fi	
		

#               
# TODO: switch when master is DOWN
#
  

cpt=10

while [ $cpt -gt 0 ]; do 
	echo "Will proceed in $cpt seconds, CTRL+C to abort ..."
	echo "---------------------------------------------"
	cpt=$(($cpt - 1))
	sleep 1

done

		echo "LOCK MASTER ...."
        mysql -f  -h "$OLD_MASTER" $CREDENTIAL << EOF
FLUSH TABLES WITH READ LOCK;
SET GLOBAL read_only = ON;
quit
EOF

	#stop slave IO
        echo "Stop I/O Thread - Connection stopped with the master"
        mysql -f  -h "$NEW_MASTER" $CREDENTIAL << EOF
SET GLOBAL read_only = ON;
RESET MASTER;
STOP SLAVE IO_THREAD;
quit
EOF
        # On attend que le thread SQL finisse de traiter le relay-log
        # Has read all relay log; waiting for the slave I/O thread to update it
        TIMEOUT=60
        echo "Waiting Relay log bin to finish proceed (TIMEOUT = ${TIMEOUT}sec)"
        i=0
        while : ; do
                if [ "$i" -gt "$TIMEOUT" ] ; then
                        echo "waiting, SLAVE to finish all his operations.!!!"
                        break
                fi
                mysql -f -h "$NEW_MASTER" $CREDENTIAL -e 'SHOW PROCESSLIST\G' | grep -qi 'Has read all relay log; waiting for the slave I/O thread to update it'
                if [ "$?" -eq "0" ] ; then
                        break
                else
                        echo -n "."
                fi
                i=$(($i + 1))
                sleep 1
        done

        mysql -f -h "$NEW_MASTER" $CREDENTIAL << EOF
STOP SLAVE SQL_THREAD;
RESET SLAVE ALL;
RESET MASTER;
quit
EOF

        BINLOG_FILE=$(mysql -f -h "$NEW_MASTER" $CREDENTIAL -e 'SHOW MASTER STATUS\G' | grep -i 'File' | awk '{ print $2 }')
                echo "getting FILE BIN LOG FROM NEW MASTER...: $BINLOG_FILE"
        BINLOG_POS=$(mysql -f -h "$NEW_MASTER" $CREDENTIAL -e 'SHOW MASTER STATUS\G' | grep -i 'Position' | awk '{ print $2 }')
                echo "getting BINLOG POSITION FROM NEW MASTER..:$BINLOG_POS "
        mysql -f -h "$NEW_MASTER" $CREDENTIAL << EOF
SET GLOBAL read_only = OFF;
quit
EOF


        # switch the old master to slave
        echo "SETTING CONFIG IN THE NEW SLAVE (OLD MASTER)..."
       mysql -f -h "$OLD_MASTER" $CREDENTIAL << EOF
STOP SLAVE;
RESET SLAVE;
RESET MASTER;
CHANGE MASTER TO MASTER_HOST='$NEW_MASTER', MASTER_USER='$REPLICATION_USER', MASTER_PASSWORD='$REPLICATION_PASS', MASTER_LOG_FILE='$BINLOG_FILE', MASTER_LOG_POS=$BINLOG_POS;
START SLAVE;
UNLOCK TABLES;
quit
EOF


}

#
# Main
#
while getopts "cm:s:h" option; do

        case "${option}" in
          s)
                OLD_MASTER="${OPTARG}"
                ;;
          m)
                NEW_MASTER="${OPTARG}"
                ;;
	  h)	
		usage
		;;
	  c)
		CONFIG=1
		;;
          *)
                usage
                ;;
        esac
done


if [ $CONFIG = 1 ]; then 

	get_actual_config

else
# DO SWITCH

  shift $((OPTIND-1))

	if [ -z "${OLD_MASTER}" ] || [ -z "${NEW_MASTER}" ]; then
                usage
	fi
   echo ""
   echo "******************BASCULE MARIA-DB*******************"
   echo ""
   echo "NEW SLAVE: ${OLD_MASTER}"
   echo "NEW MASTER: ${NEW_MASTER}"
   read -p "Are you sure to  switch the master from ${OLD_MASTER} to ${NEW_MASTER} and the SLAVE from ${NEW_MASTER} to ${OLD_MASTER} ? Y/N: " -n 1 -r
   echo 
	if [[ $REPLY =~ ^[Yy]$ ]] ; then
	    echo "Starting ..."
    		check_mysql_connect "$NEW_MASTER"
    		check_mysql_connect "$OLD_MASTER"

		# STRAT SWITCH
  		switch_slave_master

	 	if [ $SKIP_TEST = 0 ] ; then 
		echo  
		   test_replication
		fi
	fi
fi
