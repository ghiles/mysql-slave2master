# SLAVE2MASTER
Script bash permettant d'inverser la réplication mysql, et élire le SLAVE comme master.

## Utilisation
```
bash slave2master.sh  [-m <new master>] [-s <new slave>]

```
à la fin de l'operation, le script effectue un test de réplication, en créant une table test.test, insert un RANDUM num, et verifie si cette tabe est répliqué sur le slave.
